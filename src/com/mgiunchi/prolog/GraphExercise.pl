%   Dati un insieme di fatti che descrivono un grafo aciclico caratterizzato da un solo
%   nodo iniziale (con soli archi uscenti) ed almeno un nodo finale, il predicato "linked"
%   risulta vero se tutti gli stati finali sono raggiungibili dallo stato iniziale.

nodes([0,1,2,3,4]).
start(0).
final([3,4]).
edge(0,1).
edge(0,2).
edge(1,3).
edge(2,3).
edge(2,4).

linked:- start(I), final(F), reachable(I, F). % I deve essere iniziale, F finale e raggiungibile da I
reachable(_, []).
reachable(I, [H | T]):- rag(I, H), reachable(I, T). %la lista [H|T] è raggiungibile da I se da I riesco arrivare in H e da I riesco ad arrivare a T
rag(I, I). %auto anello
rag(I, S):- edge(I, S1), rag(S1, S). %nodo I collegato a S1 ed S1 deve essere collegato con S con almeno un arco

%   Data una base di fatti che specifica un grafo aciclico e diretto, il predicato "leaves(F)"
%   risulta vero quando F è una lista costituita esclusivamente da nodi foglia del grafo.

edge(a,c).
edge(b,d).
edge(c,d).
edge(d,e). %e foglia
edge(d,f). %f foglia

leaf([]).
leaf([H|T]):- edge(_,H), not(edge(H,_)), leaf(T). %per essere una foglia deve esserci un arco entrante, nessun uscente