 % Es 1.1
 % dropAny(+Elem,+List,-OutList)
 dropAny(X,[X|T],T). %se elem in testa lo tolgo
 dropAny(X,[H|Xs],[H|L]):- dropAny(X,Xs,L). %

 % drops only the first occurrence (showing no alternative results)
 % dropFirst(+Elem,+List,-OutList)
 dropFirst(X,[X|T],T):- !.
 dropFirst(X,[H|Xs],[H|L]):- dropFirst(X,Xs,L).

 % drops only the last occurrence (showing no alternative results)
 % dropLast(+Elem,+List,-OutList)
 dropLast(X,[H|Xs],[H|L]):-dropLast(X,Xs,L), !. %arrivato in fondo all'albero di ricerca eseguo ! e blocco altre unificaz.
 dropLast(X,[X|T],T).

 %dropAll: drop all occurrences, returning a single list as result
 dropAll(X, [], []).
 dropAll(X,[X|T],L):- !,dropAll(X, T, L). %se elem è in testa, tolgo elem dalla testa (L coda senza X)
 dropAll(X,[H|T],[H|L]):- dropAll(X, T, L). %se elem non è in testa, tolgo elem dalla coda (L coda senza X)

 %It obtains a graph from a list
 % fromList(+List,-Graph)
 fromList([_],[]).
 fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L). %i valori H1,H2,T corrispondono al grafo(H1)->grafo(H2)->L, se H2,T corrisponde grafo(H2)->L

 %Es. 2.2
 % fromCircList(+List,-Graph)
 fromList([_],[]).
 fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).
 fromCircList([X|T],G):-append([X|T],[X],Y),fromList(Y,G). %aggiungo la testa in coda e genero il grafo

 %Es. 2.4
 % reaching(+Graph, +Node, -List)
 % all the nodes that can be reached in 1 step from Node
 reaching(G, N, L):- reaching(G, N, L, []). %aggiungo una lista dove porre risultato
 %Se "e" ha il valore pari a N ricercato, aggiungo X (collegamento) alla lista NewL e proseguo con altri nodi
 reaching([e(N, X)|T], N, L, Temp):- !,append([X], Temp, NewL), reaching(T, N, L, NewL). %senza ! ottengo anche soluz parziali
 %Se "e" ha il valore pari a X1 diverso da N, proseguo con altri nodi
 reaching([e(X1, X)|T], N, L, Temp):- reaching(T, N, L, Temp).
 reaching([], N, L, Temp):- L = Temp. %quando ho esaurito la lista dei nodi, resitutisco il risultato

 %Es 2.5
 % anypath(+Graph, +Node1, +Node2, -ListPath)
 % a path from Node1 to Node2
 % if there are many path, they are showed 1-by-1
 %Se il nodo è quello sorgente, applico la ricerca sugli altri nodi cercando il percorso verso destinazione, aggiungo nodo a lista LP
 anypath([e(FROM, N)|T], FROM, TO, LP):- anypath(T, N, TO, CurrentLP), append([e(FROM, N)], CurrentLP, LP), member(e(_, TO), LP).% il member serve per avere percorsi completi, ossia che comprendono sempre la destinazione.
 %Se il nodo è quello di destinazione, applico nuova ricerca sui altri nodi
 anypath([e(N, TO)|T], FROM, TO, LP):- anypath(T, FROM, TO, CurrentLP), append([e(N, TO)], CurrentLP, LP), member(e(_, N), LP). % il member server per avere percorsi privi di buchi (tra nodo sorgente e destinazione)
 anypath([H|T], FROM, TO, LP):- anypath(T, FROM, TO, LP). %scorro la lista se H non è nodo cercato
 anypath([], FROM, TO, LP):- LP = [].

 %Es 2.6
 % allreaching(+Graph, +Node, -List)
 % all the nodes that can be reached from Node
 % Suppose the graph is NOT circular!
 % Use findall and anyPath!
  allreaching(Graph, Node, List):- findall(Destination, anypath(Graph, Node, Destination, ListPath), List).