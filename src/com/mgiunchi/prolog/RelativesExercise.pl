%   Dato il seguente elenco di fatti, determinare i predicati in grado di determinare
%   se sussistono i seguenti rapporti: fratello, cugino, nonno, antenato, parente.

parent(guido, elena).
parent(guido, luisa).
parent(elena, giovanni).
parent(elena, paola).
parent(luisa, andrea).
parent(luisa, dario).

brother(X,Y):- parent(Z,X),parent(Z,Y),X\=Y.
cousin(X,Y):- parent(Z1,X),parent(Z2,Y),brother(Z1,Z2).
grandfather(X,Y):- parent(X,Z), parent(Z,Y).
ancestor(X,X).
ancestor(X,Y):- parent(X,Z), ancestor(Z,Y).
relatives(X,Y):- ancestor(Z, X), ancestor(Z,Y).

%   Dato il seguente elenco di fatti, determinare i predicati in grado di determinare
%   se sussistono i seguenti rapporti: nonno, antenato.

parent(andrea, [lucia, mino]).
parent(lucia, [anna, enzo, marta]).
parent(mino, [sandra, giuseppe, tina]).
parent(anna, [emma]).
parent(marta, [luisa, lino, sofia]).

grandfather(X,Y):-parent(X, L1), member(Z, L1), parent(Z, L2), member(Y, L2).
ancestor(X, Y):-parent(X, L), member(Y, L). %father
ancestor(X, Y):-parent(X, L), member(Z, L), ancestor(Z, Y).

%   Supponendo di avere una base di fatti relativa alla specifica di una serie di famiglie, (ogni fatto specifica
%   il padre, la madre ed i figli di una famiglia), il predicato "cousinMother(X,Y") risulta vero qualora X ed Y
%   siano cugini di primo  grado, entrambi da parte di madre.

family(guido, ester, [franco, nella, elena]). %padre, madre e lista figli
family(bruno, nella, [andrea, dario, zeno]).
family(franco, lucia, [giovanni, paola, letizia, sofia]).
family(antonio, elena, [francesco, maria, maddalena]).


cousinMother(X,Y):- family(P1,M1,F1), family(P2,M2,F2),
P1 \= P2, %padri differenti
member(X, F1), member(Y, F2),%X figlio di M1,P1 e Y figlio di M2,P2
family(_,_,F), %deve esistere una lista di figli F tale che
member(M1, F), member(M2, F). %entrambi le madri vi appartengono (sorelle)


