%   Il seguente predicato restituisce True, qualora le due liste poste in ingresso
%   siano prive di elementi in comune

%   Soluzione 1
without_common_elements([], _).
without_common_elements([H1|L1], L2):- not member(H1,L2),without_common_elements(L1, L2).

%   Soluzione 2
no_common_elements(List1, List2):- not (member(X, List1), member(X, List2)).%estraggo tutti gli elem e per ognuno deve verificari il pred

%   Il seguente predicato"even_odd(List,Even,Odd)", data una lista di interi, restituisce due liste
%   di cui Even contiene solo elementi pari, mentre Odd gli elementi dispari

even_odd([], [], []).
even_odd([H|T], [H|Even], Odd):- 0 is H mod 2, !,even_odd(T, Even, Odd).
even_odd([H|T], Even, [H|Odd]):- 1 is H mod 2, even_odd(T, Even, Odd).


%   Il seguente predicato "double_len(List1,List2)", date due liste in ingresso, verifica che List1 abbia
%   lunghezza doppia rispetto a quella di List2

%   Soluzione 1
len([],0).
len([H|T],N):-len(T,N1), N is N1+1.
double_len(X,Y):-len(X,A),len(Y,B), A is 2*B.

%   Soluzione 2
double_len([],[]).
double_len([H11,H12|T1], [H2|T2]):-double_len(T1, T2).

%   Il seguente predicato, data una lista caratteri, verifica se costituisce una stringa palindroma

%   Soluzione 1
palindroma([]).%una lista vuota è palindroma
palindroma([_]). %una lista con un solo elemento è palind.
palindroma([Head|List]) :-append(CenterList,[Head],List),palindroma(CenterList).

%   Soluzione 2
palindroma(List):-reverse(List,List).
reverse([],[]).
reverse([Head|Tail],RevList):-append(PartReverse,[Head],RevList), reverse(Tail, PartReverse).

%   Il seguente predicato, data una lista interi, calcola la media.

averageList(List, Average ):-
    sum(List, Sum),
    length(List, Length),
    Length > 0,
    Average is Sum / Length.
sum([],0).
sum([H|T],S):- sum(T,TailSum), S is H+TailSum.

%   Il predicato "sumcouple", data una lista di numeri, restituisce una lista costituita
%   dalla somma delle coppie della lista 2 a 2 (nel caso di elementi in numero dispari
%   l'ultimo elemento rimane disacoppiato).

sumcouple([],[]):-!.
sumcouple([X],[X]):-!.
sumcouple([X,Y|T],[Z|SP]):- Z is X+Y, sumcouple(T,SP).

%   Il predicato square, dato un numero intero N, restituisce la lista de quadrati
%   dei numeri compresi tra 1 a N.

square(0,[]):-!.
square(N,Q):- N1 is N-1, NN is N*N, square(N1,Q1), append(Q1,[NN],Q).

%   Il seguente predicato "insertInSortedList(N,L1,L2)", risulta vero qualora N sia un numero intero,
%   L1 una lista ordinata (in modo crescente, eventualmente vuota) di numeri interi,
%   ed L2 la lista ordinata ottenuta inserendo N in L1.

insertInSortedList(N,L1,L2):- sortedList(L1), insert(N,L1,L2), sortedList(L2).
%verifica se la lista è ordinata
sortedList(X):- var(X), !. %testa se x è una var
sortedList([]):- !.
sortedList([_]):- !.
sortedList([N1,N2|T]):- N1 =< N2, sortedList([N2|T]).
insert(N,[],[N]) :- !. %se lista vuota, solo elem
insert(N,[H|T],[N,H|T]) :- N =< H, !. %ins in testa
insert(N,[H|T],[H|Z]) :- insert(N,T,Z). %se N>H, va inserito nella coda Z
