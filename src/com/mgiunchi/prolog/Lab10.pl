 %Es 1.3
 % search_two(Elem,List)
 % looks for two occurrences of Elem with an element in between!
 search3(X,[X,_,X|_]).
 search3(X,[_|T]):-search3(X,T).

 %Es 1.4
 % search_anytwo(Elem,List)
 % looks for any Elem that occurs two times
 search(X,[X|_]).
 search(X,[_|T]):-search(X,T).
 search_anytwo(X,[X|T]):-search(X,T).
 search_anytwo(X,[_|T]):-search_anytwo(X,T).

 %Es 2.2
 % size(List,Size)
 % Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..
 size([],zero).
 size([_|T],s(X)):-size(T,X).

 %Es 2.3
 % sum(List,Sum)
 sum([],0).
 sum([H|T],M):-sum(T,N), M is N+H.

 %Es 2.5
 % max(List,Max)
 % Max is the biggest element in List
 % Suppose the list has at least one element
 max([H|T],M):-max(T,H,M).
 max([],TEMP,TEMP). %fine: assegno a max TEMP
 max([X|T],TEMP,M):- X>TEMP, max(T,X,M).
 max([X|T],TEMP,M):- X=<TEMP, max(T,TEMP,M).

 %Es. 3.2
 % all_bigger(List1,List2)
 % all elements in List1 are bigger than those in List2, 1 by 1
 all_bigger([H1],[H2]):- H1>H2.
 all_bigger([H1|T1],[H2|T2]):- H1>H2, all_bigger(T1,T2).

 %Es. 3.3
 % sublist(List1,List2)
 % List1 should be a subset of List2
 sublist([],_).
 sublist([H1|T1],[H2|T2]):- member(H1,[H2|T2]), sublist(T1,[H2|T2]).

 search(X,[X|_]).
 search(X,[_|T]):-search(X,T).
 sublist([H1],[H2|T2]):-search(H1,[H2|T2]).
 sublist([H1|T1],[H2|T2]):-search(H1,[H2|T2]),sublist(T1,[H2|T2]).

 %Es. 4.2
 % seqR(N,List)
 % example: seqR(4,[4,3,2,1,0]).
 seqR(0,[0]).
 seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

 %Es. 4.3
 % seqR2(N,List)
 seqR2(N,[M|T]):- N > 0, M == 0, N2 is M+1, seqR2(N,T,N2). %aggiungo un campo
 seqR2(N,[N],N). %caso base (il nuovo campo ha lo stesso valore di n: lo inserisco nella lista)
 seqR2(N,[M|T],M):- NewN2 is M+1, seqR2(N,T,NewN2). %aggiungo elementi minori in testa 

 %Es. Inverse List
 % inv(List,List)
 % example: inv([1,2,3],[3,2,1]).
 inv([],[]).
 inv([H|T],RevList):-append(PartReverse,[H],RevList), inv(T, PartReverse).

 %Es.
 % double(List,List)
 % example: double([1,2,3],[1,2,3,1,2,3]).
 double([],[]).
 double([H|T],[H|T1]):-append([H|T],[H|T],[H|T1]).

 %Es.
 % times(List,N,List)
 % example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
 times(List, N, Result):- times(List, N, Result, []). %aggiungo un campo dove porre il risultato
 times(List, 0, Result, Temp):- Result = Temp. %se N==0, copio temp in result
 times(List, N, Result, Temp):- N > 0, M is N-1, append(Temp, List, NewResult), times(List, M, Result, NewResult).

 %Es.
 % proj(List,List)
 % example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
 proj(List, Result):- proj(List, Result, []). %aggiungo un campo temp pari a lista vuota, dove porre il risultato
 proj([], Result, Temp):- Result = Temp. %se lista inizale vuota, copio temp in result
 proj([[H|T]|Others], Result, Temp):- append(Temp, [H], NewResult), proj(Others, Result, NewResult).